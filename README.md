# auto-feature-branch-deploy

This is a proof of concept demo application which will create a new CD twin repo for each branch named feature/*<branch name>*
This is the CD repository of the auto-feature-branch poc code.

The CI repository can be found here: https://gitlab.com/remmen/demos/auto-feature-branch

```Please note, that this is a simplified proof of concept code!```


## Structure

The deployment is based on a classical kustomize structure with a base and overlays for each environment. The argocd folder contains the application definition.
```
├── README.md
├── argocd
│   ├── feature
│   │   └── demo-app-feature.yml
│   └── main
│       └── demo-app.yml
└── kustomize
    ├── base
    │   ├── deployment.yml
    │   ├── kustomization.yml
    │   └── service.yml
    └── overlay
        ├── feature
        │   ├── basic-auth-sealed.yml
        │   ├── ingress.yml
        │   └── kustomization.yml
        └── main
            ├── basic-auth-sealed.yml
            ├── ingress.yml
            └── kustomization.yml

```
## Prerequisites
You need to create two sealed secrets 

- a secret allowing argocd to read the repository (if it's private)
- a secret named basic-auth for the htaccess password protection

To create a htaccess password use 
```htpasswd -nb demo demo | base64```

In order to use this code, you also need to replace the git-server URL als well as the domains in the ingress definitions.

## Variables
Communication to the kubernetes cluster is based on tokens. Under *settings/ci_cd* within your GitLab environment you need to create the following variables.

| variable  | description  |
|---|---|
| **ARGO_GIT_URL** | git url to be used   |
| **ARGO_TOKEN** | access token to argo |
| **ARGO_URL** | management url of argo |


## Gitlab environments

The state of each branch is tracked trough a gitlab environment. Once this branch get deleted trough the stop action of the CI pipeline, the stop action of this pipeline will delete the argocd application which will delete all the kubernetes ressources due the finalizer "resources-finalizer.argocd.argoproj.io"
